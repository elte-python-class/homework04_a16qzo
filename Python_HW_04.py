# Converting data

# import json
# import numpy as np

# data = json.load(open("/home/course/data_files/Sensdogneck_Mano_2017-05-11_12_37_42.071.json"))

# data["data"].keys()

# data["data"]["attitude"]

# dataArr = np.array(data["data"]["attitude"])

# print(dataArr)

# print(type(dataArr))
# print(len(dataArr))



# Creating a class

# class arrayClass:
#    import json
#    import numpy as np
#    
#    def __init__(self, path, measurement):
#        self.path = path
#        self.measurement = measurement
#    
#    def loadArray(self):
#        data = json.load(open(self.path))
#        dataArr = np.array(data["data"][self.measurement])
#        return dataArr

# attitude = arrayClass("/home/course/data_files/Sensdogneck_Mano_2017-05-11_12_37_42.071.json", "attitude")

# attArray = attitude.loadArray()

# print(attArray)



# Creating a module

import sys

sys.path.append("/home/a16qzo/pythoncodes")

import arraymodule

type(arraymodule)

type(arraymodule.arrayClass)

attitude = arraymodule.arrayClass("/home/course/data_files/Sensdogneck_Mano_2017-05-11_12_37_42.071.json", "attitude")

attArray = attitude.loadArray()

print(attArray)