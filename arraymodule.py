import json
import numpy as np

class arrayClass:   
    
    def __init__(self, path, measurement):
        self.path = path
        self.measurement = measurement
    
    def loadArray(self):
        data = json.load(open(self.path))
        dataArr = np.array(data["data"][self.measurement])
        return dataArr